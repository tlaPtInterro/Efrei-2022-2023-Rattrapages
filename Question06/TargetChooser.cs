﻿using Question06.Console.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Question06.Console
{
    public class TargetChooser
    {
        private readonly TextWriter writer;
        public TargetChooser(TextWriter writer)
        {
            this.writer = writer;
        }

        public void WriteTop5Criminals(Bounty[] bounties)
        {
            // Exclure les primes "impossibles"
            var filteredBounties = bounties.Where(b => b.Danger != Danger.Impossible);

            // Grouper par criminel et calculer la somme des valeurs des primes pour chaque criminel
            var groupedCriminals = filteredBounties
                .GroupBy(b => b.Person)
                .Select(g => new { Criminal = g.Key, TotalValue = g.Sum(b => b.Value) });

            // Sélectionner les 5 criminels les plus intéressants en fonction de la somme des valeurs des primes
            var top5Criminals = groupedCriminals.OrderByDescending(c => c.TotalValue).Take(5);

            // Écrire les informations dans le writer
            writer.WriteLine("Top 5 criminals:");
            foreach (var criminalInfo in top5Criminals)
            {
                writer.WriteLine($"Criminal: {criminalInfo.Criminal.FirstName} {criminalInfo.Criminal.LastName}, Total Reward: {criminalInfo.TotalValue}");
                var criminalBounties = filteredBounties.Where(b => b.Person == criminalInfo.Criminal);
                foreach (var bounty in criminalBounties)
                {
                    writer.WriteLine($"- Bounty: {bounty.Reward}, Danger: {bounty.Danger}, City: {bounty.City.Name}, DeadOrAlive: {bounty.DeadOrAlive}");
                }
            }
        }
    }
}



// using Question06.Console.Entities;

// namespace Question06.Console;

// public class TargetChooser
// {
//     private readonly TextWriter writer;
//     public TargetChooser(TextWriter writer)
//     {
//         this.writer = writer;
//     }

//     public void WriteTop5Criminals(Bounty[] bounties)
//     {
//         throw new NotImplementedException();
//     }
// }
