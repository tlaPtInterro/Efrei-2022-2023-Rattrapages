﻿namespace Question06.Console.Entities;

public enum City
{
    Paris,
    Tokyo,
    London,
    Berlin,
    Lisbon
}
