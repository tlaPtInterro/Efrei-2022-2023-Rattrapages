﻿namespace Question06.Console.Entities
{
    public record Bounty(
        Person Person,
        int Reward,
        Danger Danger,
        City City,
        bool DeadOrAlive
    )
    {
        // Propriété Value calculée
        public double Value => Reward * (CalculateDanger() * CalculateDeadOrAlive());

        // Méthode privée pour calculer le facteur de danger
        private double CalculateDanger()
        {
            return Danger switch
            {
                Danger.Low => 1.2,
                Danger.Medium => 1,
                Danger.Hard => 0.7,
                Danger.Impossible => 0.2,
                _ => throw new ArgumentException("Invalid danger level"),
            };
        }

        // Méthode privée pour calculer le facteur de DeadOrAlive
        private double CalculateDeadOrAlive()
        {
            return DeadOrAlive ? 1 : 0.75;
        }
    }
}



// namespace Question06.Console.Entities;

// public record Bounty(
//     Person Person,
//     int Reward,
//     Danger Danger,
//     City City,
//     bool DeadOrAlive, 
//     double value
//     )
// {
//     public double Value
//         => throw new NotImplementedException();

//     public double GetCalculateValue () {

//         return Reward*(calculateDanger()*calculatedeadOrAlive());

//     }

//     public double calculateDanger () {
//         double dangerValue = 0;
//         switch(Danger){
//             case Low : 
//                 dangerValue=1.2;
//                 break;
//             case Medium: 
//                 dangerValue=1;
//                 break;
//             case Hard: 
//                 dangerValue=0.7;
//                 break;
//             case Impossible: 
//                 dangerValue=0.2;
//                 break;
//         }
//         return dangerValue;
//     }

//     public double calculatedeadOrAlive () {
//         double deadOrAlive = 0;

//         if (this.DeadOrAlive) {
//             deadOrAlive = 1 ;
//         }else {
//             deadOrAlive = 0.75;
//         }

//         return deadOrAlive;
//     }

// }
