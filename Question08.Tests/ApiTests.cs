using Microsoft.AspNetCore.Mvc.Testing;
using Question08.Api;
using System.Net.Http.Json;

namespace Question08.Tests;

public class ArtifactsTest : IClassFixture<WebApplicationFactory<Program>>
{
    private readonly WebApplicationFactory<Program> factory;

    public ArtifactsTest(WebApplicationFactory<Program> factory)
    {
        this.factory = factory.WithWebHostBuilder(_ => { });
    }

    [Fact]
    public async Task StartsEmpty()
    {
        var getResponse = await factory.CreateClient().GetAsync("pirateShips");
        getResponse.EnsureSuccessStatusCode();

        var pirateShips = await getResponse.Content.ReadAsStringAsync();
        Assert.Equal("[]", pirateShips);
    }

    [Fact]
    public async Task PutThenGetAll()
    {
        await PutShip("The Black Pearl", 42);

        string json = await GetAllShips();
        Assert.Equal($"[{GetShipJson("The Black Pearl", 42)}]", json);
    }

    [Fact]
    public async Task PutThenGetSingle()
    {
        await PutShip("The Black Pearl", 42);

        string json = await GetShip("The Black Pearl");
        Assert.Equal(GetShipJson("The Black Pearl", 42), json);
    }

    private async Task PutShip(string name, int crewSize)
    {
        var putResponse = await factory.CreateClient().PutAsJsonAsync($"pirateShips/{name}", new
        {
            name,
            crewSize
        });
        putResponse.EnsureSuccessStatusCode();

        var response = await putResponse.Content.ReadAsStringAsync();
        Assert.Equal(GetShipJson(name, crewSize), response);
    }

    private static string GetShipJson(string name, int crewSize)
        => $"{{\"name\":\"{name}\",\"crewSize\":{crewSize}}}";

    private async Task<string> GetAllShips()
    {
        var getResponse = await factory.CreateClient().GetAsync("pirateShips");
        getResponse.EnsureSuccessStatusCode();

        return await getResponse.Content.ReadAsStringAsync();
    }

    private async Task<string> GetShip(string name)
    {
        var getResponse = await factory.CreateClient().GetAsync($"pirateShips/{name}");
        getResponse.EnsureSuccessStatusCode();

        return await getResponse.Content.ReadAsStringAsync();
    }

    [Fact]
    public async Task PutThreeAndGetAndGetAll()
    {
        await PutShip("The Black Pearl", 42);
        await PutShip("The Walrus", 28);
        await PutShip("The Wicked Wench", 37);

        Assert.Equal(GetShipJson("The Black Pearl", 42), await GetShip("The Black Pearl"));
        Assert.Equal(GetShipJson("The Walrus", 28), await GetShip("The Walrus"));
        Assert.Equal(GetShipJson("The Wicked Wench", 37), await GetShip("The Wicked Wench"));

        var ships = await GetAllShips();
        Assert.Equal("[" + string.Join(",",
            GetShipJson("The Black Pearl", 42),
            GetShipJson("The Walrus", 28),
            GetShipJson("The Wicked Wench", 37)) + "]", ships);
    }

}
